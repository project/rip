<h1>Remove Invalid Permissions (RIP)</h1>
<h2>Table of Contents</h2>
<ul>
    <li><a href="#introduction">Introduction</a></li>
    <li><a href="#requirements">Requirements</a></li>
    <li><a href="#installation">Installation</a></li>
    <li><a href="#usage">Usage</a></li>
</ul>
<h2 id="introduction">Introduction</h2>
<p>The <strong>Remove Invalid Permissions (RIP)</strong> module is designed to simplify the process of removing invalid permissions in Drupal 9. It addresses the need to clean up invalid permissions before upgrading to Drupal 10, ensuring a smoother transition.</p>
<h2>Requirements</h2>
<p>Before you begin, ensure you meet the following requirements:</p>
<ul>
  <li>Drupal 8 or 9 installed.</li>
  <li>Appropriate permissions to configure modules and administer permissions.</li>
  <li>Drush installed (optional).</li>
  <li>Configuration synchronization module installed (optional).</li>
</ul>
<h2>Installation</h2>
<ul>
  <li>Install via <em>Composer</em>:
    <ul>
      <li>Install module with Composer <code>composer require drupal/rip</code>.</li>
      <li>Enable with the Drush command <code>drush en rip</code>.
    </ul>
  </li>
  <li>Alternatively download the module and activate it under <em>Admin > Extend</em>.</li>
</ul>
<h2>Usage</h2>
<p>To use the <strong>RIP</strong> module, follow these steps:</p>
<ol>
  <li>Enable the module as described in the Installation section.</li>
  <li>Remove the invalid permissions:
    <ul>
      <li>via Drush: Run <code>drush rip</code>.</li>
      <li>via Admin UI: Navigate to <em>Admin > People > RIP</em> and press <em>Submit</em>.</li>
    </ul>
  </li>
  <li>Export the configuration for other environments (optional):
    <ul>
      <li>via Drush: Run <code>drush cex</code>.</li>
      <li>via Admin UI: Navigate to <em>Administration > Configuration > Development > Configuration synchronization > Export</em> and press <em>Export</em>.</li>
    </ul>
  </li>
  <li>Uninstall the Remove Invalid Permissions module:
    <ul>
      <li>via Drush: Run <code>drush un rip</code>.</li>
      <li>via Admin UI: Navigate to <em>Admin > Extend > Uninstall</em>, select the Remove Invalid Permissions module and press <em>Uninstall</em>.</li>
    </ul>
  </li>
</ol>
<hr>

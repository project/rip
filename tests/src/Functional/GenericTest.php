<?php

namespace Drupal\Tests\rip\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module test for rip.
 *
 * @group rip
 */
class GenericTest extends GenericModuleTestBase {

  /**
   * {@inheritdoc}
   */
  protected function assertHookHelp(string $module): void {
    // Don't do anything here. We intend to implement hook_help() differently.
  }

}
